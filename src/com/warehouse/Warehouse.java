package com.warehouse;

import java.util.ArrayList;
import java.util.List;

public final class Warehouse {
    private List<Room> rooms;

    public Warehouse(List<Room> rooms) {
        this.rooms = rooms;
    }

    public List<Box> addBoxes(List<Box> boxes) {
        List<Box> rejectedBoxes = new ArrayList<>();
        for(Box box : boxes) {
            Room firstAvailableRoom = findFirstAvailableRoom(box);
            if(firstAvailableRoom != null) {
                firstAvailableRoom.addBox(box);
            } else {
                rejectedBoxes.add(box);
            }
        }
        return rejectedBoxes;
    }

    private Room findFirstAvailableRoom(Box box) {
        for(Room room : rooms) {
            if(isRoomEligible(room, box)) {
                return room;
            }
        }
        return null;
    }

    private boolean isRoomEligible(Room room, Box box) {
        int boxVolume = box.getVolumeInSquareMeters();
        if(roomHasCapacity(room, boxVolume)) {
            if (isBoxTooHeavyForStairs(boxVolume) && room.hasStairs()) {
                return false;
            } else if(boxContainsChemicals(box) && !roomApprovedForChemicals(room)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    private boolean roomApprovedForChemicals(Room room) {
        if(room.getHazmatFlags() == HazmatFlags.CHEMICAL) {
            return true;
        }
        return false;
    }

    private boolean boxContainsChemicals(Box box) {
        if(box.getHazmatFlags() == HazmatFlags.CHEMICAL) {
            return true;
        }
        return false;
    }

    private boolean isBoxTooHeavyForStairs(int boxVolume) {
        return boxVolume > 50;
    }

    private boolean roomHasCapacity(Room room, int boxVolume) {
        int totalBoxVolume = 0;
        for(Box storedBox : room.getBoxes()) {
            totalBoxVolume += storedBox.getVolumeInSquareMeters();
        }
        return room.getVolumeInSquareMeters() - totalBoxVolume >= boxVolume;
    }
}
